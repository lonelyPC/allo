package tetris;

import java.util.Random;
import java.awt.Point;

public class Figure {
	private Point[] _figureCoordinate;
	private int _currentState = 0;
	private int _currentFigure = FigureType.T;
	private int _width = 0;
	
	Figure(int width){
		_width = width;
		generateNewFigure();
	}
	
	Figure(int width, Point[] figureCoordinate, int currentFigure, int currentState){
		_width = width;
		_figureCoordinate = figureCoordinate;
		_currentFigure = currentFigure;
		_currentState = currentState;
	}
	
	public Point[] getCoordinate(){
		return _figureCoordinate;
	}
	
	public int getCurrentState(){
		return _currentState;
	}
		
	public void generateNewFigure(){
		Random random = new Random();
		switch(random.nextInt(FigureType.SIZE)){
		case FigureType.T:
			_figureCoordinate = new Point[]{new Point(_width / 2, 0),new Point(_width / 2 - 1, 1),new Point(_width / 2, 1),new Point(_width / 2 + 1, 1)};//_|_
			_currentState = FigureState.FIRST;
			_currentFigure = FigureType.T;
			break;
		case FigureType.O:
			_figureCoordinate = new Point[]{new Point(_width / 2, 0),new Point(_width / 2 + 1, 0),new Point(_width / 2, 1),new Point(_width / 2 + 1, 1)};//o
			_currentState = FigureState.FIRST;
			_currentFigure = FigureType.O;
			break;
		case FigureType.I:
			_figureCoordinate = new Point[]{new Point(_width / 2, 0),new Point(_width / 2, 1),new Point(_width / 2, 2),new Point(_width / 2, 3)};//----
			_currentState = FigureState.FIRST;
			_currentFigure = FigureType.I;
			break;
		case FigureType.S:
			_figureCoordinate = new Point[]{new Point(_width / 2 - 1, 1),new Point(_width / 2, 1),new Point(_width / 2, 0),new Point(_width / 2 + 1, 0)};//_|-
			_currentState = FigureState.FIRST;
			_currentFigure = FigureType.S;
			break;
		case FigureType.Z:
			_figureCoordinate = new Point[]{new Point(_width / 2 - 1, 0),new Point(_width / 2, 0),new Point(_width / 2, 1),new Point(_width / 2 + 1, 1)};//-|_
			_currentState = FigureState.FIRST;
			_currentFigure = FigureType.Z;
			break;
		case FigureType.J:
			_figureCoordinate = new Point[]{new Point(_width / 2 - 1, 0),new Point(_width / 2 - 1, 1),new Point(_width / 2, 1),new Point(_width / 2 + 1, 1)};//|__
			_currentState = FigureState.FIRST;
			_currentFigure = FigureType.J;
			break;
		case FigureType.L:
			_figureCoordinate = new Point[]{new Point(_width / 2 + 1, 0), new Point(_width / 2 - 1, 1),new Point(_width / 2, 1),new Point(_width / 2 + 1, 1)};//__|
			_currentState = FigureState.FIRST;
			_currentFigure = FigureType.L;
			break;
		}
	}
	
	public void rotate(){
		Figure rotatedFigure = getRotatedFigure();
		_figureCoordinate = rotatedFigure.getCoordinate();
		_currentState = rotatedFigure.getCurrentState();
	}
	
	public Figure getRotatedFigure(){
		
		Point[] rotatedCoord;		
		switch(_currentFigure){
		case FigureType.T:
			switch(_currentState){
			case FigureState.FIRST:
				rotatedCoord = new Point[]{new Point(_figureCoordinate[0].x, _figureCoordinate[0].y),
											new Point(_figureCoordinate[1].x ,_figureCoordinate[1].y - 2),
											new Point(_figureCoordinate[2].x - 1, _figureCoordinate[2].y - 1),
											new Point(_figureCoordinate[3].x - 2, _figureCoordinate[3].y)};
				return new Figure(_width, rotatedCoord, _currentFigure, FigureState.SECOND);
			case FigureState.SECOND:
				rotatedCoord = new Point[]{new Point(_figureCoordinate[0].x, _figureCoordinate[0].y),
											new Point(_figureCoordinate[1].x + 2,_figureCoordinate[1].y),
											new Point(_figureCoordinate[2].x + 1, _figureCoordinate[2].y - 1),
											new Point(_figureCoordinate[3].x, _figureCoordinate[3].y - 2)};
				return new Figure(_width, rotatedCoord, _currentFigure, FigureState.THIRD);
			case FigureState.THIRD:
				rotatedCoord = new Point[]{new Point(_figureCoordinate[0].x, _figureCoordinate[0].y),
											new Point(_figureCoordinate[1].x, _figureCoordinate[1].y + 2),
											new Point(_figureCoordinate[2].x + 1, _figureCoordinate[2].y + 1),
											new Point(_figureCoordinate[3].x + 2, _figureCoordinate[3].y)};
				return new Figure(_width, rotatedCoord, _currentFigure, FigureState.FOURTH);
			case FigureState.FOURTH:
				rotatedCoord = new Point[]{new Point(_figureCoordinate[0].x, _figureCoordinate[0].y),
											new Point(_figureCoordinate[1].x - 2,_figureCoordinate[1].y),
											new Point(_figureCoordinate[2].x - 1, _figureCoordinate[2].y + 1),
											new Point(_figureCoordinate[3].x, _figureCoordinate[3].y + 2)};
				return new Figure(_width, rotatedCoord, _currentFigure, FigureState.FIRST);
			}
			break;
		case FigureType.I:
			switch(_currentState){
			case FigureState.FIRST:
				rotatedCoord = new Point[]{new Point(_figureCoordinate[0].x - 2, _figureCoordinate[0].y + 2),
											new Point(_figureCoordinate[1].x - 1, _figureCoordinate[1].y + 1),
											new Point(_figureCoordinate[2].x, _figureCoordinate[2].y),
											new Point(_figureCoordinate[3].x + 1, _figureCoordinate[3].y - 1)};
				return new Figure(_width, rotatedCoord, _currentFigure, FigureState.SECOND);
			case FigureState.SECOND:
				rotatedCoord = new Point[]{new Point(_figureCoordinate[0].x + 2, _figureCoordinate[0].y - 2),
											new Point(_figureCoordinate[1].x + 1, _figureCoordinate[1].y - 1),
											new Point(_figureCoordinate[2].x, _figureCoordinate[2].y),
											new Point(_figureCoordinate[3].x - 1, _figureCoordinate[3].y + 1)};
				return new Figure(_width, rotatedCoord, _currentFigure, FigureState.FIRST);
			}
			break;
		case FigureType.S:
			switch(_currentState){
			case FigureState.FIRST:
				rotatedCoord = new Point[]{new Point(_figureCoordinate[0].x, _figureCoordinate[0].y - 2),
											new Point(_figureCoordinate[1].x - 1, _figureCoordinate[1].y - 1),
											new Point(_figureCoordinate[2].x, _figureCoordinate[2].y),
											new Point(_figureCoordinate[3].x - 1, _figureCoordinate[3].y + 1)};
				return new Figure(_width, rotatedCoord, _currentFigure, FigureState.SECOND);
			case FigureState.SECOND:
				rotatedCoord = new Point[]{new Point(_figureCoordinate[0].x, _figureCoordinate[0].y + 2),
											new Point(_figureCoordinate[1].x + 1, _figureCoordinate[1].y + 1),
											new Point(_figureCoordinate[2].x, _figureCoordinate[2].y),
											new Point(_figureCoordinate[3].x + 1, _figureCoordinate[3].y - 1)};
				return new Figure(_width, rotatedCoord, _currentFigure, FigureState.FIRST);
			}
			break;
		case FigureType.Z:
			switch(_currentState){
			case FigureState.FIRST:
				rotatedCoord = new Point[]{new Point(_figureCoordinate[0].x + 2, _figureCoordinate[0].y - 1),
											new Point(_figureCoordinate[1].x + 1, _figureCoordinate[1].y),
											new Point(_figureCoordinate[2].x, _figureCoordinate[2].y - 1),
											new Point(_figureCoordinate[3].x - 1, _figureCoordinate[3].y)};
				return new Figure(_width, rotatedCoord, _currentFigure, FigureState.SECOND);
			case FigureState.SECOND:
				rotatedCoord = new Point[]{new Point(_figureCoordinate[0].x - 2, _figureCoordinate[0].y + 1),
											new Point(_figureCoordinate[1].x - 1, _figureCoordinate[1].y),
											new Point(_figureCoordinate[2].x, _figureCoordinate[2].y + 1),
											new Point(_figureCoordinate[3].x + 1, _figureCoordinate[3].y)};
				return new Figure(_width, rotatedCoord, _currentFigure, FigureState.FIRST);
			}
			break;
		case FigureType.J:
			switch(_currentState){
			case FigureState.FIRST:
				rotatedCoord = new Point[]{new Point(_figureCoordinate[0].x + 1, _figureCoordinate[0].y - 1),
											new Point(_figureCoordinate[1].x,_figureCoordinate[1].y - 2),
											new Point(_figureCoordinate[2].x - 1, _figureCoordinate[2].y - 1),
											new Point(_figureCoordinate[3].x - 2, _figureCoordinate[3].y)};
				return new Figure(_width, rotatedCoord, _currentFigure, FigureState.SECOND);
			case FigureState.SECOND:
				rotatedCoord = new Point[]{new Point(_figureCoordinate[0].x + 1, _figureCoordinate[0].y + 1),
											new Point(_figureCoordinate[1].x + 2,_figureCoordinate[1].y),
											new Point(_figureCoordinate[2].x + 1, _figureCoordinate[2].y - 1),
											new Point(_figureCoordinate[3].x, _figureCoordinate[3].y - 2)};
				return new Figure(_width, rotatedCoord, _currentFigure, FigureState.THIRD);
			case FigureState.THIRD:
				rotatedCoord = new Point[]{new Point(_figureCoordinate[0].x - 1, _figureCoordinate[0].y + 1),
											new Point(_figureCoordinate[1].x, _figureCoordinate[1].y + 2),
											new Point(_figureCoordinate[2].x + 1, _figureCoordinate[2].y + 1),
											new Point(_figureCoordinate[3].x + 2, _figureCoordinate[3].y)};
				return new Figure(_width, rotatedCoord, _currentFigure, FigureState.FOURTH);
			case FigureState.FOURTH:
				rotatedCoord = new Point[]{new Point(_figureCoordinate[0].x - 1, _figureCoordinate[0].y - 1),
											new Point(_figureCoordinate[1].x - 2,_figureCoordinate[1].y),
											new Point(_figureCoordinate[2].x - 1, _figureCoordinate[2].y + 1),
											new Point(_figureCoordinate[3].x, _figureCoordinate[3].y + 2)};
				return new Figure(_width, rotatedCoord, _currentFigure, FigureState.FIRST);
			}
			break;
		case FigureType.L:
			switch(_currentState){
			case FigureState.FIRST:
				rotatedCoord = new Point[]{new Point(_figureCoordinate[0].x - 1, _figureCoordinate[0].y + 1),
											new Point(_figureCoordinate[1].x ,_figureCoordinate[1].y - 2),
											new Point(_figureCoordinate[2].x - 1, _figureCoordinate[2].y - 1),
											new Point(_figureCoordinate[3].x - 2, _figureCoordinate[3].y)};
				return new Figure(_width, rotatedCoord, _currentFigure, FigureState.SECOND);
			case FigureState.SECOND:
				rotatedCoord = new Point[]{new Point(_figureCoordinate[0].x - 1, _figureCoordinate[0].y - 1),
											new Point(_figureCoordinate[1].x + 2,_figureCoordinate[1].y),
											new Point(_figureCoordinate[2].x + 1, _figureCoordinate[2].y - 1),
											new Point(_figureCoordinate[3].x, _figureCoordinate[3].y - 2)};
				return new Figure(_width, rotatedCoord, _currentFigure, FigureState.THIRD);
			case FigureState.THIRD:
				rotatedCoord = new Point[]{new Point(_figureCoordinate[0].x + 1, _figureCoordinate[0].y - 1),
											new Point(_figureCoordinate[1].x, _figureCoordinate[1].y + 2),
											new Point(_figureCoordinate[2].x + 1, _figureCoordinate[2].y + 1),
											new Point(_figureCoordinate[3].x + 2, _figureCoordinate[3].y)};
				return new Figure(_width, rotatedCoord, _currentFigure, FigureState.FOURTH);
			case FigureState.FOURTH:
				rotatedCoord = new Point[]{new Point(_figureCoordinate[0].x + 1, _figureCoordinate[0].y + 1),
											new Point(_figureCoordinate[1].x - 2,_figureCoordinate[1].y),
											new Point(_figureCoordinate[2].x - 1, _figureCoordinate[2].y + 1),
											new Point(_figureCoordinate[3].x, _figureCoordinate[3].y + 2)};
				return new Figure(_width, rotatedCoord, _currentFigure, FigureState.FIRST);
			}
			break;
		}

		return new Figure(_width, _figureCoordinate, _currentFigure, FigureState.SECOND);
		
	}
	
	public Figure getMovedFigure(Point MOVE){
		Point[] movedCoord = new Point[]{new Point(),new Point(),new Point(),new Point()};
		for(int i = 0; i < 4; i++){
			movedCoord[i].y = getCoordinate()[i].y + MOVE.y;
			movedCoord[i].x = getCoordinate()[i].x + MOVE.x;
		}
		return new Figure(_width, movedCoord, _currentFigure, _currentState);
	}
	
	public void moveFigure(Point MOVE){
		for(int i = 0; i < 4; i++){
			_figureCoordinate[i].y = getCoordinate()[i].y + MOVE.y;
			_figureCoordinate[i].x = getCoordinate()[i].x + MOVE.x;
		}
	}
	
}