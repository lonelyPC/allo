package tetris;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JFrame;

public class MyFrame extends JFrame implements KeyListener{
	
	private static GameModel _model;
	private int[][] _matrix;
	private int _scale;
	
	public void paint(Graphics g){
		
		for(int i = 0; i < _matrix.length ; i++){
			for(int j = 0; j < _matrix[0].length; j++){
				switch(_matrix[i][j]){
				case 0:
					g.setColor(Color.WHITE);
					g.fillRect(j*_scale, i*_scale, _scale, _scale);
					break;
				case 1:
					g.setColor(Color.YELLOW);
					g.fillRect(j*_scale, i*_scale, _scale, _scale);
					break;
				case 2:
					g.setColor(Color.cyan);
					g.fillRect(j*_scale, i*_scale, _scale, _scale);
					break;
				}
			}
		}
	}
	
	public MyFrame(int scale, GameModel model){
		super("Tetris");
		_matrix = model.getMatrix();
		_scale = scale;
		_model = model;
		setSize(_model.getWidth()*scale, _model.getHeight()*scale);
		this.setResizable(false);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		addKeyListener(this);
	}
	
	public void setMatrix(int[][] matrix){
		_matrix = matrix;
	}
	
	@Override
	public void keyPressed(KeyEvent e) {
		switch(e.getKeyCode()){
		case KeyEvent.VK_A:
			_model.moveFigure(Move.LEFT);
			break;
		case KeyEvent.VK_D:
			_model.moveFigure(Move.RIGHT);
			break;
		case KeyEvent.VK_W:
			_model.rotate();
			break;
		case KeyEvent.VK_S:
			_model.moveFigure(Move.DOWN);
			break;
		}		
	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}
}
