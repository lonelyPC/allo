package tetris;

import java.awt.Point;
import java.awt.event.KeyEvent;

public interface GameModel {
	void rotate();
	int[][] getMatrix();
	void nextTick();
	void moveFigure(Point MOVE);
	String getGameName();
	int getWidth();
	int getHeight();
}
