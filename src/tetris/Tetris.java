package tetris;


import java.awt.Point;
import java.awt.event.KeyEvent;

public class Tetris implements GameModel {
	private static int WIDTH;
	private static int HEIGHT;
	private int[][] _matrix;
	private Figure _figure;
	private  String _name = "Tetris";
	
	Tetris(int height, int width){
		WIDTH = width;
		HEIGHT = height;
		_matrix = new int[height][width];
		_figure = new Figure(WIDTH);
		drawFigure(_figure);
	}

	@Override
	public void rotate() {
		Figure rotatedFigure = _figure.getRotatedFigure();
		if(!getCollisionCondition(rotatedFigure)){
			eraseFigure(_figure);
			drawFigure(rotatedFigure);
			_figure.rotate();
		}
	}

	private void drawFigure(Figure figure) {
		_matrix[figure.getCoordinate()[0].y][figure.getCoordinate()[0].x] = 1;
		_matrix[figure.getCoordinate()[1].y][figure.getCoordinate()[1].x] = 1;
		_matrix[figure.getCoordinate()[2].y][figure.getCoordinate()[2].x] = 1;
		_matrix[figure.getCoordinate()[3].y][figure.getCoordinate()[3].x] = 1;
	}

	private void eraseFigure(Figure figure) {
		_matrix[figure.getCoordinate()[0].y][figure.getCoordinate()[0].x] = 0;
		_matrix[figure.getCoordinate()[1].y][figure.getCoordinate()[1].x] = 0;
		_matrix[figure.getCoordinate()[2].y][figure.getCoordinate()[2].x] = 0;
		_matrix[figure.getCoordinate()[3].y][figure.getCoordinate()[3].x] = 0;
	}
	
	private void frozeFigure(Figure figure) {
		_matrix[figure.getCoordinate()[0].y][figure.getCoordinate()[0].x] = 2;
		_matrix[figure.getCoordinate()[1].y][figure.getCoordinate()[1].x] = 2;
		_matrix[figure.getCoordinate()[2].y][figure.getCoordinate()[2].x] = 2;
		_matrix[figure.getCoordinate()[3].y][figure.getCoordinate()[3].x] = 2;
	}

	private boolean getCollisionCondition(Figure rotatedFigure) {
		if	(
				_matrix.length != rotatedFigure.getCoordinate()[0].y &&
				_matrix.length != rotatedFigure.getCoordinate()[1].y &&
				_matrix.length != rotatedFigure.getCoordinate()[2].y &&
				_matrix.length != rotatedFigure.getCoordinate()[3].y && 
			(_matrix[rotatedFigure.getCoordinate()[0].y][rotatedFigure.getCoordinate()[0].x] == 0 || _matrix[rotatedFigure.getCoordinate()[0].y][rotatedFigure.getCoordinate()[0].x] == 1) &&
			(_matrix[rotatedFigure.getCoordinate()[1].y][rotatedFigure.getCoordinate()[1].x] == 0 || _matrix[rotatedFigure.getCoordinate()[1].y][rotatedFigure.getCoordinate()[1].x] == 1) &&
			(_matrix[rotatedFigure.getCoordinate()[2].y][rotatedFigure.getCoordinate()[2].x] == 0 || _matrix[rotatedFigure.getCoordinate()[2].y][rotatedFigure.getCoordinate()[2].x] == 1) &&
			(_matrix[rotatedFigure.getCoordinate()[3].y][rotatedFigure.getCoordinate()[3].x] == 0 || _matrix[rotatedFigure.getCoordinate()[3].y][rotatedFigure.getCoordinate()[3].x] == 1) ){
			return false;} else {
				return true;}
	}

	@Override
	public int[][] getMatrix() {
		return _matrix;
	}
	
	@Override
	public void nextTick() {
		checkEndCondition();
		moveFigure(Move.DOWN);	
	}
	
	private void checkExplosiveLine() {
		int counter;
		for(int u = 0; u < 4; u++){
			counter = 0;
		for(int i = 0; i < _matrix.length; i++){
			for(int j = 0; j < _matrix[0].length; j++){
				if (_matrix[i][j] == 2){
					counter++;
				} else {
					counter = 0;
					break;
				}
			}
			if (counter == _matrix[0].length){
				for(int k = i; k >= 1; k--){
					for(int m = 0; m < _matrix[0].length; m++){
						_matrix[k][m] = _matrix[k - 1][m];
					}
				}
					}
			}
		}
	}
	

	@Override
	public void moveFigure(Point MOVE) {
		
		Figure movedFigure = _figure.getMovedFigure(MOVE);
		
		if	(!getCollisionCondition(movedFigure)){
			
			eraseFigure(_figure);
			
			drawFigure(movedFigure);
			
			_figure.moveFigure(MOVE);
			} else {
				if(MOVE == Move.DOWN){
				frozeFigure(_figure);
				checkExplosiveLine();
				_figure.generateNewFigure();
				}
			}
		
	}

	private void checkEndCondition() {
		for(int i = 0; i < WIDTH; i++){
			if (_matrix[0][i] != 0 && _matrix[0][i] != 1){
				System.exit(0);
			}
		}
	}
	
	@Override
	public String getGameName() {
		return _name;
	}

	@Override
	public int getWidth() {
		return WIDTH;
	}

	@Override
	public int getHeight() {
		return HEIGHT;
	}
	

}
