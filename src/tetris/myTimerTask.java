package tetris;

import java.util.TimerTask;

public class myTimerTask extends TimerTask {
	
	private GameModel _model;
	private int[][] _matrix;
	private MyFrame _jframe;
	private long _time;
	myTimerTask(GameModel model, MyFrame jframe){
		_model = model;
		_jframe = jframe;
		_time = System.currentTimeMillis();
	}
	
	@Override
	public void run() {
		if(System.currentTimeMillis() - _time > 1000){
		_model.nextTick();
		_time = System.currentTimeMillis();
		}
		render(_model.getMatrix());
	}
	
	private void render(int[][] matrix){
		_jframe.setMatrix(matrix);
		_jframe.repaint();		
	}
}
