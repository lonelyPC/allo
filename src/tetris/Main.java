package tetris;

import java.util.Timer;

public class Main  {
	private static GameModel _model;
	private static final int WIDTH = 10;
	private static final int HEIGHT = 20;
	private static final int SCALE = 40;
	private static final int FPS = 1000 / 60;
	private static final int DELAY = 0;
	
	public static void main(String[] args) {
		_model = new Tetris(HEIGHT, WIDTH);
		MyFrame jframe = new MyFrame(SCALE, _model);
		Timer timer = new Timer();
		timer.schedule(new myTimerTask(_model,jframe), DELAY, FPS);
	}
}
